import treehash from 'treehash'
import crypto from 'crypto'
import date from 'dayjs'
import AWS from 'aws-sdk'
import fs from 'fs-extra'

try {
  await Promise.resolve()
} catch (e) {
  console.error(`Please update to node 14`)
}

const {
  AWS_ACCOUNT_ID,
  ARCHIVE_PATH
} = process.env

let {
  PART_SIZE
} = process.env

const vaultName = process.argv[2]


PART_SIZE = PART_SIZE || `1073741824`

const glacier = new AWS.Glacier({
  region: `us-east-1`
})

const datedVaultName = `${vaultName}`

try {
  console.log(`creating vault named ${datedVaultName}`)

  const vault = await new Promise((resolve, reject) => {
    glacier.createVault({
      accountId: AWS_ACCOUNT_ID,
      vaultName: datedVaultName
    }, (err, data) => {

      if (err) return reject(err)

      return resolve(data)
    })
  })

  console.log(`initiating multipart upload`)

  const { uploadId } = await new Promise((resolve, reject) => {
    glacier.initiateMultipartUpload({
      accountId: `-`,
      vaultName: datedVaultName,
      archiveDescription: `archive for ${vaultName}`,
      // 1 GiB
      partSize: PART_SIZE
    }, (err, data) => {

      if (err) return reject(err)

      return resolve(data)
    })
  })

  const body = await fs.readFile(ARCHIVE_PATH)

  let partBegin = 0

  console.log("BODY:", body.length)
  const uploads = []

  while (partBegin < body.length) {

    console.log("UPLOAD:", partBegin)
    uploads.push(new Promise((resolve, reject) => {

      ((bytesBegin) => {
        glacier.uploadMultipartPart({
          accountId: AWS_ACCOUNT_ID,
          uploadId,
          vaultName: datedVaultName,
          body,
          range: `bytes ${Math.min(bytesBegin, Math.max(body.length - PART_SIZE - 1, 0))}-${Math.min(bytesBegin + PART_SIZE, body.length - 1)}/*`
        }, (err, result) => {

          console.log("UPLOADED:", result, err)
          if (err) return reject(err)

          return resolve(result)
        })
      })(partBegin)
    }))

    partBegin = partBegin + PART_SIZE
  }

  await Promise.all(uploads)

  console.log(`finishing multipart upload`)
  glacier.completeMultipartUpload({
    accountId: AWS_ACCOUNT_ID,
    uploadId,
    archiveSize: String(body.length),
    checksum: treehash.getTreeHashFromBuffer(body),
    vaultName: datedVaultName
  }, (err, data) => {
    console.log("FIN:", data, err)

    return process.exit(0)
  })
} catch (e) {
  console.error(`error occurred uploading archive`, e)
}
