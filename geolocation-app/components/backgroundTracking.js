import React, { Component } from 'react'
import { Alert } from 'react-native'
import RNLocation from 'react-native-location';
import EnvironmentManager from 'react-native-env';

class BgTracking extends Component {
  componentDidMount() {
    RNLocation.configure({
      distanceFilter: 5.0,
      desiredAccuracy: {
        ios: "best",
        android: "balancedPowerAccuracy"
      },
      androidProvider: "auto",
      interval: 60000,
      fastestInterval: 30000,
      allowsBackgroundLocationUpdates: true,
    })

    const geoToken = EnvironmentManager.getSync('GEO_TOKEN')

    const hasPermissions = RNLocation.checkPermission({
      ios: 'always', // or 'always'
      android: {
        detail: 'fine' // or 'fine'
      }
    });

    if (!hasPermissions) {
      RNLocation.requestPermission({
        ios: 'always',
        android: {
          detail: 'fine',
          rationale: {
            title: "We need to access your location",
            message: "We use your location to show where you are on the map",
            buttonPositive: "OK",
            buttonNegative: "Cancel"
          }
        }
      });
    }

    this.locationUnsubscribe = RNLocation.subscribeToLocationUpdates(location => {
      // Who cares about the response. Just fire away.
      fetch(`https://geo.maddie.today/api/v1/add`, {
        method: `POST`,
        headers: {
          'Content-Type': `application/json`,
          Authorization: `Bearer ${geoToken}`
        },
        body: JSON.stringify(location)
      })
    })
  }

  componentWillUnmount() {
    this.locationUnsubscribe()
  }

  render() {
    return null
  }
}

export default BgTracking
