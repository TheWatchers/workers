const moment = require(`moment`)
const fetch = require(`node-fetch`)

const {
  AW_URL: awUrl,
  GRAPH_URL: graphUrl,
  EXISTIO_URL: existIOUrl,
  GRAPH_AUTHORIZATION,
  EXIST_AUTHORIZATION
} = process.env

const findBucketType = (uncleanBucketName) => {
  const splitBucket = uncleanBucketName.split(`-`)

  // aw-watcher-[bucketName]_[computer name]
  const [ bucketName, computerName ] = splitBucket[2].split(`_`)

  return bucketName
}

const relevantDate = moment().subtract(1, `day`).startOf(`day`).format()
const endOfRelevantDate = moment().subtract(1, `day`).endOf(`day`).format()

const getQuery = `
{
  activityWatch {
    window:bucket(id:"aw-watcher-window_maddie-pc") {
      id
      activity(date:"${relevantDate}") {
        id
        duration
        timestamp
        data {
          app
          title
          status
          url
          audible
          tabCount
          productivity
        }
      }
    }
    firefox:bucket(id:"aw-watcher-web-firefox") {
      id
      activity(date:"${relevantDate}") {
        id
        duration
        timestamp
        data {
          app
          title
          status
          url
          audible
          tabCount
          productivity
        }
      }
    }
  }
}
`

;(async () => {
  // Send in the graph request as a POST request
  // This might be premature optimization since I believe
  // the request limit is only applied to the query and not the response
  const graphRes = await fetch(`${graphUrl}/graphql`, {
    method: `POST`,
    body: JSON.stringify({
      query: getQuery
    }),
    headers: {
      Authorization: `Bearer ${GRAPH_AUTHORIZATION}`,
      'Content-Type': `application/json`
    }
  })

  const { data: { activityWatch } } = await graphRes.json()

  const bucketEvents = [ activityWatch.window, activityWatch.firefox ]

  // Tally up all of the categorized time across all of the buckets
  const timeCategorizations = bucketEvents.reduce((totalTimes, { activity }) => {
    const watcherTime = (activity || []).reduce((times, { duration, data: { productivity } }) => {
      times[productivity] += duration || 0

      return times
    }, { productive: 0, distracting: 0, neutral: 0, gaming: 0, tv: 0, mobile_screen: 0 })

    totalTimes.productive += watcherTime.productive || 0
    totalTimes.distracting += watcherTime.distracting || 0
    totalTimes.neutral += watcherTime.neutral || 0
    totalTimes.gaming += watcherTime.gaming || 0
    totalTimes.tv += watcherTime.tv || 0
    totalTimes.mobile_screen += watcherTime.mobile_screen || 0

    return totalTimes
  }, { productive: 0, distracting: 0, neutral: 0, gaming: 0, tv: 0, mobile_screen: 0 })


  const respPromises = Object.keys(timeCategorizations).map((key) => {
    const body = {
      name: `${key}_min`,
      value: Math.floor(timeCategorizations[key] / 60),
      date: moment(relevantDate).format(`YYYY-MM-DD`)
    }

    // TODO Use the GraphQL mutation
    // TODO Send multiple at a time
    return fetch(`${existIOUrl}/api/attributes/update`, {
      method: `PUT`,
      body: JSON.stringify(body),
      headers: {
        Authorization: `Bearer ${GRAPH_AUTHORIZATION}`,
        'x-exist-access-token': EXIST_AUTHORIZATION,
        'Content-Type': `application/json`
      }
    })
  })

  const responses = await Promise.all(respPromises)

  responses.forEach(async (res) => {
    console.log(await res.text())
  })
})()


